# Pi quiz

[Simple web app](https://maksrawski.gitlab.io/pi-quiz/) to test how many digits of pi can you remember / SPEEDRUN.

Author's best time: [2.2s for 20 digits](https://youtu.be/6C6_0jYElIw)
