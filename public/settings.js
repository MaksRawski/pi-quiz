let keys = ["id", "time", "score", "mistakes"];
let lastID = JSON.parse(localStorage.scores)["data"].length; // get the number of the stored scores
// lastID = Math.max.apply(Math, JSON.parse(localStorage.scores)["data"].map(x=>x.id)) // get last id 

// modal
let closeSettings = () => {
  document.getElementsByClassName("modal")[0].classList = "modal modal-closed";
};

document.getElementsByClassName("close")[0].onclick = closeSettings; 
document.body.addEventListener("keydown", (e) => {if (e.key == "Escape") closeSettings()})

let stopTimer = () => {
  clearInterval(Timer);
  Timer = NaN;

  // add to scoreboard
  scores["data"].push({
    "id": ++lastID,
    "time": document.getElementById("msg").innerText,
    "score": currentDigit,
    "mistakes": numOfMistakes
  })

  // save in local storage
  localStorage.scores = JSON.stringify(scores);
};

document.getElementById("three").onclick = () => {
  if (!isNaN(Timer)) stopTimer();
  else {
    document.getElementsByClassName("modal")[0].classList = "modal modal-open";
    document.getElementById("set-digits").value = stopOnDigit;

    document.getElementById("scoreboard-data").innerHTML = "";
    // scoreboard load data
    for (let i = 0; i < scores["data"].length; i++) {
      buffer = "<tr>";
      for (let j of keys) {
        buffer += "<td>";
        buffer += scores["data"][i][j];
        buffer += "</td>";
      }
      buffer += "</tr>";
      document.getElementById("scoreboard-data").innerHTML += buffer;
    }
  }
}

// settings
document.getElementById("set-digits").addEventListener("keyup", () => {
  // check whether input given is number between 1 and 300
  if (
    (isNaN(document.getElementById("set-digits").value))||
    (document.getElementById("set-digits").value > 300) ||
	(document.getElementById("set-digits").value < 1))
	{
		document.getElementById("set-digits-error").innerText = "Value should be between 1 and 300.";
	} 
  else {
    document.getElementById("set-digits-error").innerText = "";
    stopOnDigit = document.getElementById("set-digits").value;
    // save it in local storage
    localStorage.stopOnDigit = stopOnDigit;
  }
});
