let table = document.getElementById("table");
const digits = '141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513282306647093844609550582231725359408128481117450284102701938521105559644622948954930381964428810975665933446128475648233786783165271201909145648566923460348610454326648213393607260249141273';
// first 300 digits

let box = `<input class="guessing-spot" type="text"></input>`;

let currentDigit = 0;
let numOfMistakes = 0;
let start = 0;
let Timer = NaN;
let stopOnDigit = Number(localStorage.stopOnDigit) || 20;

let focusFirstElement = () => {
	table.firstChild.focus();
	if (window.innerHeight > window.innerWidth) {
		table.firstChild.blur();
	}
}

// PWA
if ('serviceWorker' in navigator) {
	navigator.serviceWorker
		.register('sw.js', {scope: './'})
		.then(_ => console.log("Service worker registered."))
		.catch(err => console.log("Failed to register service worker!", err));
}

for (digit in digits) {
	if ((digit + 1) % 10 == 0) table.innerHTML += `<br/>`;
	table.innerHTML += box;
}
focusFirstElement();

table.addEventListener('keydown', (e) => {
	if (e.key == 'r' || e.key == 'R') {e.preventDefault(); restartQuiz();}
	else if (e.key >= 0 && Array.from(table.children).indexOf(e.target) == currentDigit) {
		e.preventDefault();
		if (e.key == digits[currentDigit]) {
			e.target.value = e.key;
			e.target.nextSibling.focus();

			currentDigit++;
			refresh();
			if (currentDigit == 1) {
				//start timer
				time = 0;
				Timer = setInterval(() => {
					time++;
					document.getElementById("msg").innerText = formatTime(time);
				}, 100)
			}
			if (currentDigit == stopOnDigit) stopTimer();

			if (currentDigit % 10 == 0) {
				table.scroll({
					top: ((currentDigit + 1) / 10) * 38,
					left: 0,
					behavior: 'smooth'
				});
			}

		}
		else {
			numOfMistakes++;
			refresh();
		}
	}
	else e.preventDefault();
});

let formatTime = (time) => {
	seconds = Math.floor(time / 10);
	return seconds + "." + (time % 10);
};

enterDigit = (digit) => {
	if (digit == digits[currentDigit]) {
		table.children[currentDigit].value = digit;
		currentDigit++;
		refresh();

		if (currentDigit == 1) {
			//start timer
			time = 0;
			Timer = setInterval(() => {
				time++;
				document.getElementById("msg").innerText = formatTime(time);
			}, 100)
		}
		if (currentDigit == stopOnDigit) stopTimer();


		if (currentDigit % 10 == 0) {
			table.scroll({
				top: ((currentDigit + 1) / 10) * 38,
				left: 0,
				behavior: 'smooth'
			});
		}

	}
	else {
		numOfMistakes++;
		refresh();
	}
}

// wow i wonder whether there exists something that could automagically update that
let refresh = () => {
	let scoreSpan = document.getElementById('score').children[0];
	let mistakeSpan = document.getElementById('mistakes').children[0];

	scoreSpan.innerText = currentDigit;
	mistakeSpan.innerText = numOfMistakes;
}

let restartQuiz = () => {
	// stop timer if it's still running
	clearInterval(Timer);
	Timer = NaN;

	document.getElementById("msg").innerText = "To enter settings or see the scoreboard click the 3. above"; // reset text 
	// document.getElementsByClassName('guessing-spot')[0].focus();

	for (let i = 0; i < currentDigit; i++) {
		document.getElementsByClassName('guessing-spot')[i].value = "";
	}

	table.scroll({
		top: 0,
		left: 0,
		behavior: 'smooth'
	});

	currentDigit = 0;
	score = 0;
	numOfMistakes = 0;
	start = 0;

	refresh();
	focusFirstElement();
}

// hide the mobile numpad on desktop
if (window.innerHeight < window.innerWidth) {
	document.getElementById('mobile-numpad').style.display = "none";
}

let checkLocalScores = () => {
	try {
		JSON.parse(localStorage.scores);
	}
	catch (e) {
		if (!localStorage.scores) console.log("No scores saved");
		else console.log("Scores data is corrupted!");
		return 1; // problem with json
	}
	return 0; // everything ok
}

if (checkLocalScores() == 1) {
	defaultJSONData =
	{
		"data": []
	}
	localStorage.scores = JSON.stringify(defaultJSONData);
	scores = defaultJSONData;
}
else scores = JSON.parse(localStorage.scores);
